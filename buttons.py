from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from kivy.uix.image import Image

##############################################################################################################
#Buttons pressed or not

class StartButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(StartButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/start_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/start_button_not_pressed.png"

class OptionButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(OptionButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/options_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/options_button_not_pressed.png"

class ExitButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(ExitButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/exit_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/exit_button_not_pressed.png"

class BackButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(BackButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/back_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/back_button_not_pressed.png"

class SaveButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(SaveButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/Buttons/save_button_pressed.png"
    def on_release(self):
        self.source = "Resources/Buttons/save_button_not_pressed.png"

#####################################################################################################
#Buttons BattleScreen

class FleeButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(FleeButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/HUD/skills/flee_clicked.png"
    def on_release(self):
        self.parent.parent.reset()
        self.source = "Resources/HUD/skills/flee.png"
        #self.parent.player_source = "Resources/sprite/char/paladin/idle.gif"
        self.parent.parent.manager.transition.direction = 'left'
        self.parent.parent.manager.current = 'gameplay'

class HealButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(HealButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/HUD/skills/heal_clicked.png"
    def on_release(self):
        self.source = "Resources/HUD/skills/heal.png"
        self.parent.parent.heal()
        #self.parent.player_source = "Resources/sprite/char/paladin/hit.gif"

class BlockButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(BlockButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/HUD/skills/block_clicked.png"
    def on_release(self):
        self.source = "Resources/HUD/skills/block.png"
        self.parent.parent.defend()
        #self.parent.player_source = "Resources/sprite/char/paladin/idle.gif"

class AttackButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(AttackButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/HUD/skills/attack_clicked.png"
    def on_release(self):
        self.source = "Resources/HUD/skills/attack.png"
        self.parent.parent.attack()
        #self.parent.parent.player_source = "Resources/sprite/char/bowman/attack.gif"
        #self.parent.parent.reload_opponents()

#######################################################################################################
#Buttons where you can defined your figure

class BowmanButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(BowmanButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/sprite/char/bowman/faceset_selected.png"
        print("Archer selected")
        self.parent.parent.selected = "bowman"
    def on_release(self):
        self.source = "Resources/sprite/char/bowman/faceset.png"

class MageButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(MageButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/sprite/char/mage/faceset_selected.png"
        print("Mage selected")
        self.parent.parent.selected = "mage"
    def on_release(self):
        self.source = "Resources/sprite/char/mage/faceset.png"

class PaladinButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(PaladinButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/sprite/char/paladin/faceset_selected.png"
        print("Paladin selected")
        self.parent.parent.selected = "paladin"
    def on_release(self):
        self.source = "Resources/sprite/char/paladin/faceset.png"

class WarriorButton(ButtonBehavior, Image):
    def __init__(self, **kwargs):
        super(WarriorButton, self).__init__(**kwargs)
    def on_press(self):
        self.source = "Resources/sprite/char/warrior/faceset_selected.png"
        print("Warrior selected")
        self.parent.parent.selected = "warrior"
    def on_release(self):
        self.source = "Resources/sprite/char/warrior/faceset.png"
