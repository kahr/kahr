
#https://github.com/spinningD20/kivy_rpg/blob/master/states/turn.py

from kivy.config import Config
Config.set('graphics', 'width', '704')
Config.set('graphics', 'height', '480')
Config.set('graphics', 'position', 'custom')
Config.set('graphics', 'resizable', False)
Config.set('graphics', 'left', 100)
Config.set('graphics', 'top',  100)
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')
Config.write()

#################################################################################################

from kivy.app import App
from kivy.lang import Builder
from kivy.clock import Clock
import random
import time
from kivy.vector import Vector
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.image import Image, AsyncImage
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.properties import StringProperty
from kivy.core.window import Window
#from kivy.animation import Animation
from kivy.uix.boxlayout import BoxLayout
#from kivy.uix.textinput import TextInput
from kivy.properties import ObjectProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
#from kivy.uix.behaviors import ButtonBehavior
#from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import (NumericProperty, ReferenceListProperty,
                             ObjectProperty)

from world import world1
from world2 import world2
from buttons import *
#Builder.load_file('heme.kv')
from time import sleep


#################################################################################################

class GameTileMap(Image):
    pass

class Tile(Widget):
    pass

class MenuScreen(Screen):
    pass

class SettingsScreen(Screen):
    pass

class CharacterCreationScreen(Screen):
    pass

class Character(Widget):
    pass

class Coll(Image):
    pass

class RedPotionItem(Image):
    pass

class YellowPotionItem(Image):
    pass

class BluePotionItem(Image):
    pass

class GreenPotionItem(Image):
    pass

class StairItem(Image):
    pass

class BombItem(Image):
    pass

class ReptilianItem(Image):
    pass

#################################################################################################

gameroot = FloatLayout()
rootCollision = FloatLayout()
ItemGrid = FloatLayout()


class GameScreen(Screen):
    def __init__(self, **kwargs):
        super(GameScreen, self).__init__(**kwargs)
        self.active = 0
        self.badTiles = []
        self.RedPotionItems = []
        self.YellowPotionItems = []
        self.GreenPotionItems = []
        self.BluePotionItems = []
        self.BombItems = []
        self.Items = []
        self.tiles = [Tile(pos=(self.x, self.y))]
        self.items_list = []
        self.activeMap = world2
        self.wimg = Basic(size = (13,17),
                        pos=(200, 100),
                        size_hint=(None, None),
                        allow_stretch=True,
                        keep_ratio=False
                        )

    def playy(self):
        gameroot.add_widget(self.wimg, index = 0)
        Clock.schedule_interval(self.wimg.update, 1.0 / 60.0)

    def worldgen(self):
        self.add_widget(gameroot, index = 3)
        self.add_widget(rootCollision, index = 2)
        self.add_widget(ItemGrid, index = 1)
        #x, y = 0, 448
        mapping = {
                '0': "Resources/tile_stone.png",
                '1': "Resources/items/StairItem.png",
                '2': "Resources/items/floor_empty.png",
                '3': "Resources/items/floor_empty.png",
                '4': "Resources/items/floor_empty.png",
                '5': "Resources/items/floor_empty.png",
                '6': "Resources/items/floor_empty.png",
                '7': "Resources/items/floor_empty.png",
        }

        if 1 == 1:
            x, y = 0, 448
            for tile in self.activeMap:
                image = mapping.get(tile)
                if image:
                    tile_map = GameTileMap(
                        size = (32,32),
                        source = image,
                        pos = (x, y),
                        size_hint=(None, None),
                        allow_stretch=True,
                        keep_ratio=False
                    )
                    self.tiles.append(tile_map)

                x += 32  # jump to the next tile in the row
                if tile == '\n':
                    # Jump to the next line
                    x = 0
                    y -= 32

        if 1 == 1:
            x, y = 0, 448
            for tileI in self.activeMap:
                items = {
                        '1': StairItem,
                        '2': BombItem,
                        '3': BluePotionItem,
                        '4': GreenPotionItem,
                        '5': RedPotionItem,
                        '6': YellowPotionItem,
                        '7': ReptilianItem,
                }
                class_name = items.get(tileI)
                item_widget_name =  str(class_name)
                item_name = item_widget_name[17:-2]
                if class_name:
                    tile_map = class_name(
                        source = ('Resources/items/' + item_name + '.png'),
                        size = (32,32),
                        pos = (x, y),
                        size_hint=(None, None),
                        allow_stretch=True,
                        keep_ratio=False
                    )
                    if class_name != '':
                        self.Items.append(tile_map)


                x += 32  # jump to the next tile in the row
                if tileI == '\n':
                    # Jump to the next line
                    x = 0
                    y -= 32

        if 1 == 1:
            x, y = 0, 448
            for tileC in self.activeMap:
                items = {
                        'X': Coll,
                }
                tile_walls = items.get(tileC)
                if tile_walls:
                    tile_map = Coll(
                        source = "Resources/tile_water.png",
                        size = (32,32),
                        pos = (x, y),
                        size_hint=(None, None),
                    )
                    self.badTiles.append(tile_map)

                x += 32  # jump to the next tile in the row
                if tileC == '\n':
                    # Jump to the next line
                    x = 0
                    y -= 32

        for tile in self.tiles:
            print(tile, "added at", tile.pos)
            gameroot.add_widget(tile)

        for tile in self.badTiles:
            print(tile, "added at", tile.pos)
            rootCollision.add_widget(tile)

        for tile in self.Items:
            ItemGrid.add_widget(tile)

        #print(self.tiles)
        #print(self.badTiles)

    def on_pre_enter(self):
        print ("on_enter fired")
        if self.active == 0:
            self.worldgen()
            self.playy()
        self.active += 1

#################################################################################################

class Basic(Image):

    pFrame = 0
    SPEED = 1.2  # number of pixels character is moved on each turn.

    def __init__(self, **kwargs):
        super(Basic, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._keyboard.bind(on_key_up=self._on_keyboard_up)
        self.source = 'Resources/sprite/sprite_down.png'
        self.pressed_keys = set()

        self.pressed_actions = {
            'up': lambda: self.move_up(),
            'down': lambda: self.move_down(),
            'left': lambda: self.move_left(),
            'right': lambda: self.move_right(),
            'e': lambda: self.pickup(),
            }

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard.unbind(on_key_up=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        self.pressed_keys.add(keycode[1])

    def _on_keyboard_up(self, keyboard, keycode):
        self.pressed_keys.remove(keycode[1])

    def pickup(self):
        if self.check_tile() == 'reptilian':
            self.switch_screen()
        elif self.check_tile() == 'green':
            print('green')
        elif self.check_tile() == 'blue':
            print('blue')
        elif self.check_tile() == 'red':
            print('red')
        elif self.check_tile() == 'yellow':
            print('yellow')
        elif self.check_tile() == 'stair':
            print('stairItem')
        elif self.check_tile() == 'bomb':
            print('bomb')

    def move_up(self):
        self.y += self.SPEED
        if self.check_collision() is True:
            self.y -= self.SPEED
        self.source = ('Resources/sprite/sprite_up.png')

    def move_down(self):
        self.y -= self.SPEED
        if self.check_collision() is True:
            self.y += self.SPEED
        self.source = ('Resources/sprite/sprite_down.png')

    def move_left(self):
        self.x -= self.SPEED
        if self.check_collision() is True:
            self.x += self.SPEED
        self.source = ('Resources/sprite/sprite_left.png')

    def move_right(self):
        self.x += self.SPEED
        if self.check_collision() is True:
            self.x -= self.SPEED
        self.source = ('Resources/sprite/sprite_right.png')

    # DRY principle: Don`t repeat yourself!
    def check_collision(self):
        """
        Return True if character crashes with wall.
        """
        for bad in self.parent.parent.badTiles:
            if self.parent.parent.wimg.collide_widget(bad) is True:
                print ("collide with", bad)
                return True
        return False

    def switch_screen(self):
        if self.parent.parent.manager.current == "gameplay":
            self.parent.parent.manager.current = "battlescreen"

    def check_tile(self):
        for item in self.parent.parent.Items:
            if self.parent.parent.wimg.collide_widget(item) is True:
                item_widget_name =  str(item)
                item_name = item_widget_name[10:-26]
                print(item_name)
                if item_name == 'BluePotionItem':
                    return 'blue'
                if item_name == 'YellowPotionItem':
                    return 'yellow'
                if item_name == 'GreenPotionItem':
                    return 'green'
                if item_name == 'RedPotionItem':
                    return 'red'
                if item_name == 'BombItem':
                    return 'bomb'
                if item_name == 'StairItem':
                    return 'stair'
                if item_name == 'ReptilianItem':
                    return 'reptilian'
                    self.parent.parent.ItemGrid.remove_widget(item)


    def update(self, dt):
        for key in self.pressed_keys:
            try:
                self.pressed_actions[key]()
            except KeyError:
                print("Frame: %s Key %s pressed" % (self.pFrame, key))

        self.pFrame += 1
        #print(len(self.parent.parent.badTiles))
        #import pdb; pdb.set_trace()
        #print(self.x, self.y, self.width, self.height)
        for bad in self.parent.parent.badTiles:
            if self.parent.parent.wimg.collide_widget(bad) == True:
                print ("collide with", bad)
                self.collid = True
            elif self.parent.parent.wimg.collide_widget(bad) == False:
                self.collid = False

#################################################################################################
class Opponent(Image):
    pass

class CurrentPlayer(Image):
    pass

class BattleScreen(Screen):
    def __init__(self, **kwargs):
        super(BattleScreen, self).__init__(**kwargs)
        self.entered = False
        self.selected = 'mage'
        self.opponent_source = 'Resources/Enemys/monster/reptile/idle.gif'
        self.player_source = 'Resources/sprite/char/'+ self.selected + '/idle.gif'
        self.player_health = 10000
        self.opponent_health = 10000
        self.player_armor = 0
        self.opponent_armor = 0
        self.sprites = FloatLayout()
        self.bars = FloatLayout()
        self.enemy = None
        self.player = None
        self.player_health_bar = None
        self.opponent_health_bar = None
        self.player_armor_bar = None
        self.opponent_armor_bar = None

    def load_opponents(self):
        self.enemy = Opponent(pos=(10, 100),
                        size=(312,281),
                        allow_stretch=True,
                        size_hint=(None, None),
                        keep_ratio=False,
                        source=self.opponent_source
                        )

        self.player = CurrentPlayer(pos=(370, 100),
                            size=(312,281),
                            allow_stretch=True,
                            size_hint=(None, None),
                            keep_ratio=False,
                            source=self.player_source
                            )

        self.sprites.add_widget(self.player)
        self.sprites.add_widget(self.enemy)
        print(self.player_source)
        print(self.opponent_source)

    def reload_bars(self):
        self.sprites.remove_widget(self.player_health_bar)
        self.sprites.remove_widget(self.opponent_health_bar)
        self.load_bars()

    def load_bars(self):
        self.player_health_bar = Button(text=str(self.player_health),
                                size_hint=(.16,.16),
                                pos=(430, 400),
                                background_color=(1,0,0,1)
                                )

        self.opponent_health_bar = Button(text=str(self.opponent_health),
                                size_hint=(.16,.16),
                                pos=(40, 400),
                                background_color=(1,0,0,1)
                                )
        self.player_armor_bar = Button(text=str(self.player_armor),
                                size_hint=(.16,.16),
                                pos=(560, 400),
                                background_color=(0,0,1,1)
                                )

        self.opponent_armor_bar = Button(text=str(self.opponent_armor),
                                size_hint=(.16,.16),
                                pos=(170, 400),
                                background_color=(0,0,1,1)
                                )

        self.bars.add_widget(self.player_health_bar)
        self.bars.add_widget(self.opponent_health_bar)
        self.bars.add_widget(self.player_armor_bar)
        self.bars.add_widget(self.opponent_armor_bar)

    def healthsystem(self):
        if self.player_health <= 0:
            print('rip')
            if self.manager.current == "battlescreen":
                self.manager.current = "gameplay"
        if self.opponent_health <= 0:
            contento = Button(text='Congratulation, you won!', background_color=(0,1,0,1))
            popup = Popup(title='Victory',
                            content=contento,
                            auto_dismiss=False
                            )
            popup.open()

    def load_buttons(self):
        buttons = FloatLayout()
        self.add_widget(buttons)
        AttackBtn = AttackButton(pos=(95, 0),
                                allow_stretch=True,
                                size_hint=(.16,.16),
                                source='Resources/HUD/skills/attack.png',
                                )
        DefendBtn = BlockButton(pos=(225, 0),
                                allow_stretch=True,
                                size_hint=(.16,.16),
                                source='Resources/HUD/skills/block.png'
                                )
        HealBtn = HealButton(pos=(355, 0),
                                allow_stretch=True,
                                size_hint=(.16,.16),
                                source='Resources/HUD/skills/heal.png'
                                )
        FleeBtn = FleeButton(pos=(485, 0),
                                allow_stretch=True,
                                size_hint=(.16,.16),
                                source='Resources/HUD/skills/flee.png',
                                )
        buttons.add_widget(AttackBtn)
        buttons.add_widget(DefendBtn)
        buttons.add_widget(HealBtn)
        buttons.add_widget(FleeBtn)

    def enemy_turn(self):
        number = random.randint(0, 3)
        if number == 1:
            self.player_health -= random.randint(2000, 2500)
            self.reload_bars()
            self.healthsystem()
        elif number == 2:
            self.opponent_armor += random.randint(2000, 2500)
            self.reload_bars()
        elif number == 3:
            self.opponent_health += random.randint(1500, 2000)
            self.reload_bars()

    def attack(self):
        if self.opponent_armor <= 0:
            self.opponent_health -= random.randint(1000, 2000)
        elif self.opponent_armor > 0:
            self.opponent_armor -= random.randint(1000, 2000)
        self.reload_bars()
        self.enemy_turn()
        self.healthsystem()
    def defend(self):
        self.player_armor += random.randint(2000, 2500)
        self.reload_bars()
        self.enemy_turn()
    def heal(self):
        self.player_health += random.randint(1500, 2000)
        self.reload_bars()
        self.enemy_turn()
    def reset(self):
        self.player_health = 10000
        self.opponent_health = 10000

    def on_enter(self):
        if self.entered is False:
            self.add_widget(self.sprites)
            self.add_widget(self.bars)
        self.entered = True
        self.load_buttons()
        self.load_opponents()
        self.load_bars()

#################################################################################################
class KahrApp(App):

    title = 'kahr'
    icon = 'Resources/custoicon.png'

    def build(self):
        sm = ScreenManager()
        sm.add_widget(MenuScreen(name='menu'))
        sm.add_widget(SettingsScreen(name='settings'))
        sm.add_widget(CharacterCreationScreen(name='creationscreen'))
        sm.add_widget(GameScreen(name='gameplay'))
        sm.add_widget(BattleScreen(name='battlescreen'))
        return sm

    def save(self, plrname):
        fob = open('Resources/stats.txt','w+')
        fob.write(plrname + "\n")
        fob.close()

if __name__ == '__main__':
    KahrApp().run()
